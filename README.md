# OMR Extracter

This project is focused on providing an automated approach for extracting the data from traffic survey forms into spreadsheets for further analysis. This work can be done through optical mark recognition (OMR). Scnnable and more efficient survey forms are created and forms are filled by Paint software. Then, OMR recognition method is applied for reading the scanned form, processing it, extracting data, and recording it in .csv files.

Usually, the results of these surveys include hundreds of observations, and the manual approach for entering the data is very time-consuming. The mentioned point emphasizes the requirement of such an automated method in this field. The final software can be used by the MSU transportation research group or other parties needing it. Numpy, cv2 (OpenCV), matplotlib, pandas, imutils, seaborn are used. Previous approaches for similar tasks already exist and are used to build the foundation of the program.

---

### Presentation
Click on the picture below to review a video presentation of this project. You will be navigated to Youtube website.

[![](http://img.youtube.com/vi/WG4IboEoP6U/0.jpg)](http://www.youtube.com/watch?v=WG4IboEoP6U "")

---

### Report and Example

For a thorough report of the project and a step by step example, open `FINAL_REPORT.ipynb` and `Example.ipynb`

---

### Directoies Information

- `images/`: Contains the scanned images and the output .csv file which is result of software
- `docs/`: Contains the uto documentation of the project code
- `REPORTS/`: Contains biweekly reports and assignment of the course
- `LICENSE/`: Contains the MIT license file of the project
- `omr_extracter/`: This is the directory for the source code and test file

---

### Installation Guidelines

Three approaches can be taken to build the required environment and instalL the software. 

##### Option 1: `conda env create --prefix ./envs --file environment.yml`

##### Option 2: `make init`

If the option 1 or 2 are used. Then run the following command. to activate the created environment.

`conda activate ./envs`

##### Option 3: Using PyPi installer file to install the software. Run the following code in terminal.

`python setup.py sdist`

`pip install ./dist/Omr-Extracter-0.1.dev0.tar.gz`

---

### Usage Example

Run the following code to extract the data from the image located in `~/ocr-omr-reader/images/three` , and present summary of the recorded data. You may store the image you want to use in any location you want, but you need to input its `Path` to the `omr_extract.run_omr(Path)` function. The output will be generated as `traffic.csv` file in the same directory as the image used.

`from omr_extracter import omr_extract`

`results=omr_extract.run_omr('images/three')`

`data=results[0]`

`data.describe()`

---

### References

#### This software can be found on GitLab: <a href="https://gitlab.msu.edu/safaeiba/ocr-omr-reader.git">Omr_extracter</a>

__1-__ <a href="https://www.pyimagesearch.com/2016/10/03/bubble-sheet-multiple-choice-scanner-and-test-grader-using-omr-python-and-opencv/">Bubble Sheet Multiple Choice Scanner and Test Grader Using OMR, Python and OpenCV - PyImageSearch.</a>

__2-__ <a href="https://github.com/rbaron/omr">GitHub - Rbaron/Omr: Optical Mark Recognition in Python.</a>

