"""This module is supposed to test omr_extract.py module for a full form."""

from omr_extracter import omr_extract

def test_run_omr_full():
    """Returns the result of testing the module."""
    assert omr_extract.run_omr('images/three/')[1] == {'driver_visibility': [1, 2, 1, 1, 1, 2, 1],
                                                       'vehicle_type': [1, 2, 3, 4, 5, 3, 2],
                                                       'driver_phone_use': [1, 0, 2, 2, 1, 0, 1],
                                                       'driver_seatbelt': [1, 0, 1, 1, 1, 0, 2],
                                                       'driver_age': [3, 0, 2, 1, 2, 0, 2],
                                                       'brake_lights': [1, 2, 1, 1, 2, 2, 2],
                                                       'passenger_location': [1, 0, 1, 0, 0, 1, 0],
                                                       'lane': [2, 1, 1, 2, 2, 2, 3],
                                                       'passenger_seatbelt': [1, 1, 1, 1, 1, 2, 2]}
