"""This module is supposed to test omr_extract.py module for an empty form."""

from omr_extracter import omr_extract

def test_run_omr():
    """Returns the result of testing the module."""
    assert omr_extract.run_omr('images/two/')[1] == {'driver_visibility': [0, 0, 0, 0, 0, 0, 0],
                                                     'vehicle_type': [0, 0, 0, 0, 0, 0, 0],
                                                     'driver_phone_use': [0, 0, 0, 0, 0, 0, 0],
                                                     'driver_seatbelt': [0, 0, 0, 0, 0, 0, 0],
                                                     'driver_age': [0, 0, 0, 0, 0, 0, 0],
                                                     'brake_lights': [0, 0, 0, 0, 0, 0, 0],
                                                     'passenger_location': [0, 0, 0, 0, 0, 0, 0],
                                                     'lane': [0, 0, 0, 0, 0, 0, 0],
                                                     'passenger_seatbelt': [0, 0, 0, 0, 0, 0, 0]}
